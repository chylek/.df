#!/bin/sh

. "/etc/os-release"

CFG_PATH="/etc/sysctl.d/50-coredump.conf"
CD_PATH="/coredumps"

if [ "${ID}" != "arch" ]; then
    echo "ERROR: This script has been tested on Arch only."
    exit 1
fi

if [ $(id -u) -ne 0 ]; then
    echo "ERROR: Please run this script as root"
    exit 1
fi

mkdir "${CD_PATH}"
chmod 1777 "${CD_PATH}"

echo "kernel.core_pattern = ${CD_PATH}/core-%e-%s-%u-%g-%p-%t" >> "${CFG_PATH}"
echo "fs.suid_dumpable = 2" >> "${CFG_PATH}"

sysctl -p "${CFG_PATH}"
