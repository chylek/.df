#!/usr/bin/env sh

SCRIPT_NAME="$(basename "${0}")"

# 'dirname ${0}' is better than 'pwd' as we are not sure where the user is while
# performing the installation
REPO_PATH="$(realpath "$(dirname "${0}")")"
VIM_PLUGIN_DIR="${HOME}/.vim/bundle"
PACKAGES_TO_INSTALL="vim git bash-completion"
DOTFILES_LIST="bashrc gitconfig" # 'vimrc' will be added later
VIM_EXTENDED_CFG=0
DRY_RUN=0

show_help () {
    echo "Install dotfiles and/or packages."
    echo ""
    echo "Usage: ${SCRIPT_NAME} [OPTIONS]"
    echo ""
    echo "Available OPTIONS:"
    echo "  -h, --help         Show this message"
    echo "  -v, --vim-extras   Install extra plugins for vim"
    echo "  -d, --dry-run      Do not perform any actions but inform about them"
}

link_file () { # SOURCE_FILE DEST_FILE
    SOURCE_FILE="${REPO_PATH}/${1}"
    DEST_FILE="${HOME}/${2}"

    if [ ! -e "${DEST_FILE}" ]; then
        if [ ${DRY_RUN} -eq 1 ]; then
            echo "Soft linking '${SOURCE_FILE}' to '${DEST_FILE}'."
        else
            ln -s "${SOURCE_FILE}" "${DEST_FILE}"
            echo "${1} has been linked"
        fi
    else
        if [ -L "${DEST_FILE}" ]; then
            LINK_PATH="$(readlink "${DEST_FILE}")"

            if [ "${LINK_PATH}" = "${SOURCE_FILE}" ]; then
                echo "Link to '${SOURCE_FILE}' already exists as '${DEST_FILE}'."
            else
                echo "ERROR: '${DEST_FILE}' is an existing link to another file."
            fi
        else
            # Sometimes '.bashrc' already exists, e.g. on Ubuntu
            if [ "${2}" = ".bashrc" ]; then
                # If so then just include our config in the existing one
                if [ ${DRY_RUN} -eq 1 ]; then
                    echo "Adding '${SOURCE_FILE}' into existing '${DEST_FILE}'."
                else
                    echo ". \"${SOURCE_FILE}\"" >> "${DEST_FILE}"
                fi
            else
                echo "ERROR: '${DEST_FILE}' already exists."
            fi
        fi
    fi
}

get_arch_data () {
    INSTALLED_LIST="$(pacman -Q)"
    INSTALL_CMD="pacman -S --needed"
}

get_debian_data () {
    # TODO: 'apt list' shows a warning about 'apt' not being stable
    INSTALLED_LIST="$(apt list --installed)"
    INSTALL_CMD="apt-get install"
}

identify_os () {
    OS_ID_FILE="/etc/os-release"
    # This gives us "ID" as operating system identifier
    . "${OS_ID_FILE}"

    case "${ID}" in
        arch)
            get_arch_data
            ;;
        ubuntu)
            get_debian_data
            ;;
        *)
            echo "WARNING: No 'ID' found in ${OS_ID_FILE}. Checking 'ID_LIKE'."
            for ID_LIKE in ${ID_LIKE}; do
                case ${ID_LIKE} in
                    debian)
                        get_debian_data
                        break
                        ;;
                    *)
                        echo "ERROR: Distribution not handled: ${ID} (${ID_LIKE})"
                        exit 1
                        ;;
                esac
            done
            ;;
    esac
}

for ARG in "${@}"; do
    case "${ARG}" in
        -h|--help)
            show_help
            exit 0
            ;;
        -v|--vim-extras)
            VIM_EXTENDED_CFG=1
            ;;
        -d|--dry-run)
            DRY_RUN=1
            ;;
        *)
            echo "ERROR: Unknown option: ${ARG}"
            exit 1
            ;;
    esac
done

# Handle vim plugins
if [ ${VIM_EXTENDED_CFG} -eq 0 ]; then
    DOTFILES_LIST="${DOTFILES_LIST} vimrc-basic"
else
    DOTFILES_LIST="${DOTFILES_LIST} vimrc-extended"
    PACKAGES_TO_INSTALL="${PACKAGES_TO_INSTALL} ctags"

    link_file "vimrc-basic" ".vimrc-basic"
fi

# Install dotfiles
for DOTFILE in ${DOTFILES_LIST}; do
    link_file "${DOTFILE}" ".${DOTFILE%-*}" # Drop suffix starting with the '-'
done

# Check if any packages need to be installed
for PKG_NAME in ${PACKAGES_TO_INSTALL}; do
    CHECK_PACKAGE=$(echo "${INSTALLED_LIST}" | grep -wc "${PKG_NAME}")

    if [ ${CHECK_PACKAGE} -eq 0 ]; then
        identify_os

        # At least one required package is not installed
        if [ ${DRY_RUN} -eq 1 ]; then
            echo "Installing packages: sudo sh -c \"${INSTALL_CMD} ${PACKAGES_TO_INSTALL}\""
        else
            echo "Installing packages, root password may be requested"
            sudo sh -c "${INSTALL_CMD} ${PACKAGES_TO_INSTALL}"
        fi

        # No need to look for more not-yet-installed packages
        break
    fi
done

if [ ${VIM_EXTENDED_CFG} -eq 1 ]; then
    if [ ! -d "${VIM_PLUGIN_DIR}/Vundle.vim" ]; then
        if [ ${DRY_RUN} -eq 1 ]; then
            echo "Downloading and installing Vundle."
        else
            mkdir -p "${VIM_PLUGIN_DIR}"
            git clone "https://github.com/VundleVim/Vundle.vim.git" "${VIM_PLUGIN_DIR}/Vundle.vim"
            # Vundle was not reading the plugins, ':source %' fixed it
            vim -c source "~/.vimrc" -c PluginInstall
        fi
    else
        echo "Vundle has already been installed."
    fi
fi
