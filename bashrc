# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export COMMON_FLAGS="-mfpmath=sse -msse -msse2 -pedantic -g -Wextra -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization -Wendif-labels -Wfloat-equal -Wformat=2 -Wformat-nonliteral -Winline -Wmissing-declarations -Wno-unused-parameter -Wpointer-arith -Wstack-protector -Wswitch -Wundef -Wwrite-strings -O0 --std=c99"
export CFLAGS="${COMMON_FLAGS} -Wbad-function-cast -Wmissing-prototypes -Wnested-externs -Wshadow -Wstrict-prototypes --std=c99"
export CPPFLAGS="${COMMON_FLAGS} --std=c++0x"

export VALARGS="--leak-check=full --show-leak-kinds=all --track-origins=yes --partial-loads-ok=yes --track-fds=yes"

if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi

COLOR_PROMPT=0
case "${TERM}" in
   xterm*)
      COLOR_PROMPT=1
      ;;
   *)
      ;;
esac

if [ ${COLOR_PROMPT} -eq 1 ]; then
   PS1='[\[\e[32m\]\u@\h\[\e[00m\] \[\e[01;34m\]\D{%a} \t\[\e[00m\] \[\e[33m\]\w\[\e[00m\]] \s \n\$ '
else
   PS1='[\u@\h \D{%a} \t \w] \s \n\$ '
fi

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias jgrep='grep -rIn'

export HISTCONTROL=ignoreboth:erasedups_
